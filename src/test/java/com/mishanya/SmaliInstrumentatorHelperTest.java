package com.mishanya;

import static org.junit.Assert.*;
import org.junit.Test;

public class SmaliInstrumentatorHelperTest {

//    @Test
//    public void testCreateMethodLogger() throws Exception
//    {
//        SmaliInstrumentatorHelper obj = new SmaliInstrumentatorHelper();
//
//        String methodLogger = obj.createMethodLogger(false, "MainActivity", "testByte",
//                new String[]{"val"}, new int[]{1},
//                new String[]{"B"}) ;
//
//
//
//
//
//    }
//  TODO: Add tests for whole methods and for static methods!!!!!

    @Test
    public void testCreateInvokeOfMethodLoggerForDouble() throws Exception {
        SmaliInstrumentatorHelper obj = new SmaliInstrumentatorHelper();
        String invokeOfMethodLogger = obj.createInvokeOfMethodLogger(false,
                "Lcom/example/melon/myapplication/MainActivity;", "testDouble", new int[]{1}, new String[]{"D"});

        assertEquals(invokeOfMethodLogger,
                "    invoke-static {p0, p1, p2}, Lcom/example/melon/myapplication/MainActivity;->testDoubleLogger(Ljava/lang/Object;D)V");
    }

    @Test
    public void testCreateInvokeOfMethodLoggerForLong() throws Exception {
        SmaliInstrumentatorHelper obj = new SmaliInstrumentatorHelper();
        String invokeOfMethodLogger = obj.createInvokeOfMethodLogger(false,
                "Lcom/example/melon/myapplication/MainActivity;", "testLong", new int[]{1}, new String[]{"J"});

        assertEquals(invokeOfMethodLogger,
                "    invoke-static {p0, p1, p2}, Lcom/example/melon/myapplication/MainActivity;->testLongLogger(Ljava/lang/Object;J)V");
    }

    @Test
    public void testCreateInvokeOfMethodLoggerForByte() throws Exception {
        SmaliInstrumentatorHelper obj = new SmaliInstrumentatorHelper();
        String invokeOfMethodLogger = obj.createInvokeOfMethodLogger(false,
                "Lcom/example/melon/myapplication/MainActivity;", "testByte", new int[]{1}, new String[]{"B"});

        assertEquals(invokeOfMethodLogger, "    invoke-static {p0, p1}, Lcom/example/melon/myapplication/MainActivity;->testByteLogger(Ljava/lang/Object;B)V");
    }
}