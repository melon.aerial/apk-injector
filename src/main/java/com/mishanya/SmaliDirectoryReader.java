package com.mishanya;


import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

public class SmaliDirectoryReader {
    private String m_dir;
    private ArrayList<String> m_smaliFiles;

    SmaliDirectoryReader(String dir)
    {
        m_dir = dir;
        m_smaliFiles = new ArrayList<>();
    }

    public void readAllSmaliFilesInDir() throws IOException
    {
        File folder = new File(m_dir);
        File[] listOfFiles = folder.listFiles();
        if(listOfFiles.length > 0)
        {
            for (File file : listOfFiles)
            {
                if (file.isFile() && file.getName().endsWith(".smali"))
                    m_smaliFiles.add(file.getAbsolutePath());
            }
        }
    }

    String[] getSmaliFiles()
    {
        return m_smaliFiles.toArray(new String[0]);
    }
}