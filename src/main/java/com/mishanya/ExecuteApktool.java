package com.mishanya;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.File;
import java.nio.file.Files;
import static java.nio.file.StandardCopyOption.*;
import org.apache.commons.io.FileUtils;

public class ExecuteApktool
{
    public void decodeApkFile(String fileName) throws IOException
    {
        String result = executeCommand("apktool d " + fileName + " -f -o apkOutDir");
        if(!result.endsWith("I: Copying original files...\n"))
            throw new IOException("Error decoding apk file " + fileName +
                    " with apktool. Apktool output is : " + result);
    }

    public boolean buildApkFileAndCheckExistance(String fileName) throws IOException
    {
        buildApkFile(fileName);
        File file = new File("./apkOutDir/dist/" + fileName);

        return file.exists() && file.isFile();
    }

    public String cloneFile(String fileName) throws IOException
    {
        String srcFile = "./apkOutDir/dist/" + fileName;
        String dstFile = "instrumented_" + fileName;
        return Files.copy(new File(srcFile).toPath(), new File(dstFile).toPath(), REPLACE_EXISTING).toString();
    }

    public void signFile(String apkFilename, String keyFilename, String aliasName, String storePass) throws IOException
    {
        String command = "jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore " +
                keyFilename + " " + apkFilename + " " + aliasName + " -storepass " + storePass;
        String result = executeCommand(command);
        if(!result.contains("jar signed."))
            throw new IOException("Couldn't sign apk file. Signing command \"" + command + "\" returned: " + result);
    }

    public void buildApkFile(String fileName) throws IOException
    {
        String result = executeCommand("apktool b apkOutDir");
        if(!result.contains("I: Building apk file...\n"))
            throw new IOException("Error building apk file " + fileName +
                    " with apktool. Apktool output is : " + result);
    }

    public void cleanup() throws IOException
    {
        FileUtils.deleteDirectory(new File("./apkOutDir"));
    }

    private String executeCommand(String command) throws IOException
    {
        StringBuilder output = new StringBuilder();

        Process p;
        try
        {
            p = Runtime.getRuntime().exec(command);
            p.waitFor();
            BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String line;
            while((line = reader.readLine())!= null)
            {
                output.append(line);
                output.append("\n");
            }
        }
        catch(Exception e)
        {
            throw new IOException("Error executing command : " + command);
        }

        return output.toString();
    }
}
