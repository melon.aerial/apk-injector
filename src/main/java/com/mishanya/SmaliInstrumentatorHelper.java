package com.mishanya;

public class SmaliInstrumentatorHelper
{
    final static String indent = "    ";
    String m_traceMethodName = "";

    public void setLoggerFilePath(String loggerFilePath)
    {
        m_traceMethodName = "L" + loggerFilePath + ";->Trace(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/String;[Ljava/lang/Object;)V";
    }

    public String createMethodLogger(boolean isStatic, String className, String methodName, String[] paramNames,
                                     int[] paramRegisters, String[] paramTypes) throws Exception
    {
        String injectedMethodLogger;

        injectedMethodLogger = getMethodLoggerHeader(methodName, isStatic, paramRegisters, paramTypes, paramNames);
        injectedMethodLogger += "\n";
        injectedMethodLogger +=  getMethodLoggerBody(className, methodName, isStatic, paramNames, paramRegisters, paramTypes);
        injectedMethodLogger += "\n";
        injectedMethodLogger += getMethodEnd();

        return injectedMethodLogger;
    }

    private String getMethodLoggerBody(String className, String methodName, boolean isStatic, String[] paramNames,
                                       int[] paramRegisters, String[] paramTypes) throws Exception
    {
        String methodLoggerBody = "";
        methodLoggerBody += setClassAndMethodNameRegisters(className, methodName);
        methodLoggerBody += getParamNamesArrayInitializer(paramNames);
        methodLoggerBody += getParamObjectsArrayInitializer(paramRegisters, paramTypes);
        methodLoggerBody += getMethodTracerInvoke(isStatic);
        methodLoggerBody += getReturnVoid();

        return methodLoggerBody;
    }

    private String getParamNamesArrayInitializer(String[] paramNames)
    {
        String ret;
        ret = indent + "const/16 v2, " + getHexString(paramNames.length) + "\n";
        ret += indent + "new-array v2, v2, [Ljava/lang/String;" + "\n\n";
        for(int i = 0; i < paramNames.length; i++)
        {
            ret += indent + "const/16 v3, " + getHexString(i) + "\n";
            ret += indent + "const-string v4, \"" + paramNames[i] + "\"\n";
            ret += indent + "aput-object v4, v2, v3" + "\n\n";
        }

        return ret;
    }

    private String getParamObjectsArrayInitializer(int[] paramRegisters, String[] paramTypes) throws Exception
    {
        String ret;
        ret = indent + "const/16 v3, " + getHexString(paramRegisters.length) + "\n";
        ret += indent + "new-array v3, v3, [Ljava/lang/Object;" + "\n\n";
        for(int i = 0; i < paramRegisters.length; i++)
        {
            ret += indent + "const/16 v4, " + getHexString(i) + "\n";
            /* Need some magic for primitive types.*/
            ret += getParamObjectInV5(paramRegisters[i], paramTypes[i]);
            ret += indent + "aput-object v5, v3, v4" + "\n\n";
        }

        return ret;
    }

    private String getParamObjectInV5(int paramRegister, String paramType) throws Exception
    {
        String ret;

        /* If not primitive type.*/
        if (paramType.charAt(0) == 'L' || paramType.charAt(0) == '[')
        {
            ret = indent + "move-object/from16 v5, " + getRegName(paramRegister) + "\n";
        }
        else
        {
            String methodName;
            boolean needNextRegister = false;
            switch (paramType.charAt(0))
            {
                case 'D':
                    needNextRegister = true;
                    methodName = "Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;";
                    break;
                case 'J':
                    needNextRegister = true;
                    methodName = "Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;";
                    break;
                case 'I':
                    methodName = "Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;";
                    break;
                case 'F':
                    methodName = "Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;";
                    break;
                case 'Z':
                    methodName = "Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;";
                    break;
                case 'B':
                    methodName = "Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;";
                    break;
                case 'S':
                    methodName = "Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;";
                    break;
                case 'C':
                    methodName = "Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;";
                    break;
                default:
                    throw new Exception("undefined primitive type.");
            }

            String firstRangeReg = getRegName(paramRegister);
            String secondRangeReg = getRegName(paramRegister);
            if(needNextRegister)
                secondRangeReg = getRegName(paramRegister + 1);

            ret = indent + "invoke-static/range {" + firstRangeReg + " .. " + secondRangeReg + "}, "
                    + methodName + "\n";
            ret += indent + "move-result-object v5" + "\n";
        }

        return ret;
    }

    private String getMethodTracerInvoke(boolean isStatic)
    {
        if(isStatic)
        {
            String putNull = indent + "const/4 v6, 0x0\n";
            String invokeWithNull = indent + "invoke-static {v0, v1, v6, v2, v3}, " + m_traceMethodName + "\n\n";
            return putNull + invokeWithNull;
        }
        else
            return indent + "invoke-static {v0, v1, p0, v2, v3}, " + m_traceMethodName + "\n\n";
    }

    private String getReturnVoid()
    {
        return indent + "return-void\n";
    }

    private String setClassAndMethodNameRegisters(String className, String methodName)
    {
        String ret;
        ret = indent + "const-string v0, \"" + className + "\"\n";
        ret += indent + "const-string v1, \"" + methodName + "\"\n\n";

        return ret;
    }

    private String getHexString(int num)
    {
        return "0x" + Integer.toHexString(num);
    }

    private String getMethodLoggerHeader(String methodName, boolean isStatic, int[] paramRegisters, String[] paramTypes,
                                         String[] paramNames)
    {
        String methodDesc = getPrivateMethodLoggerDesc(methodName, isStatic, paramTypes);
        String paramLines = getMethodParamLines(paramRegisters, isStatic, paramTypes, paramNames);

        return methodDesc + paramLines;
    }

    private String getMethodParamLines(int[] paramRegisters, boolean isStatic, String[] paramTypes, String[] paramNames)
    {
        String paramLines = indent + ".locals 7\n";

        if(!isStatic)
        {
            /* For non-static method we got this pointer.*/
            paramLines += indent + ".param p0, \"obj\" # Ljava/lang/Object;\n";
        }

        for(int i = 0; i < paramRegisters.length; i++)
        {
            paramLines += indent + ".param " + getRegName(paramRegisters[i]) + ", \"" + paramNames[i]
                    + "\" # " + paramTypes[i] + "\n";
        }

        return paramLines;
    }

    private String getMethodEnd()
    {
        return ".end method\n";
    }

    public String createInvokeOfMethodLogger(boolean isStatic, String className, String methodName,
                                             int[] paramRegisters, String[] paramTypes)
    {
        String invokeText = "";
        //int numRegs = calculateNumberOfRegs(isStatic, paramTypes);
        if(isStatic && paramTypes.length == 0)
        {
            String paramRegList = getParamRegistersList(isStatic, paramRegisters, paramTypes);
            invokeText += "invoke-static {" + paramRegList + "}, " +
                    getMethodLoggerDescWithClass(className, methodName, isStatic, paramTypes);
        }
        else
        {
            String paramRegRange = getParamRegistersRange(isStatic, paramRegisters, paramTypes);
            invokeText += "invoke-static/range {" + paramRegRange + "}, " +
                    getMethodLoggerDescWithClass(className, methodName, isStatic, paramTypes);
        }

        return indent + invokeText;
    }

    private String getParamRegistersList(boolean isStatic, int[] paramRegisters, String[] paramTypes)
    {
        String paramRegList = "";
        /* For static methods v0 with null link, otherwise p0 with object link.*/
        if(!isStatic)
            paramRegList = "p0";

        /* Make list of parameters.*/
        for(int i = 0; i < paramRegisters.length; i++)
        {
            if(isStatic && i == 0)
                paramRegList += getRegName(paramRegisters[i]);
            else
                paramRegList += ", " + getRegName(paramRegisters[i]);

            /* For double and long parameters add one more register.*/
            if(paramTypes[i].equals("D") || paramTypes[i].equals("J"))
                paramRegList += ", " + getRegName(paramRegisters[i] + 1);
        }

        return paramRegList;
    }

    private String getParamRegistersRange(boolean isStatic, int[] paramRegisters, String[] paramTypes)
    {
        String paramRegRange;
        String firstReg, lastReg;
        if(isStatic)
            firstReg = getRegName(paramRegisters[0]);
        else
            firstReg = getRegName(0);

        /* If last parameters is double or long add one more register.*/
        if(paramTypes.length > 0)
        {
            if (paramTypes[paramTypes.length - 1].equals("D") || paramTypes[paramTypes.length - 1].equals("J"))
                lastReg = getRegName(paramRegisters[paramRegisters.length - 1] + 1);
            else
                lastReg = getRegName(paramRegisters[paramRegisters.length - 1]);
        }
        else
            lastReg = firstReg;

        paramRegRange = firstReg + " .. " + lastReg;
        return paramRegRange;
    }

    private String getRegName(int num)
    {
        return "p" + num;
    }

    private String getMethodLoggerDescWithClass(String className, String methodName, boolean isStatic,
                                                String[] paramTypes)
    {
        String methodLoggerDesc;

        methodLoggerDesc = className + "->" + methodName + "Logger(";

        if(!isStatic)
        {
            /* Insert object type for this or for null link.*/
            methodLoggerDesc += "Ljava/lang/Object;";
        }

        for(String param : paramTypes)
            methodLoggerDesc += param;

        methodLoggerDesc += ")V";

        return methodLoggerDesc;
    }

    private String getPrivateMethodLoggerDesc(String methodName, boolean isStatic, String[] paramTypes)
    {
        String methodLoggerDesc;

        methodLoggerDesc = ".method private static " + methodName + "Logger(";

        if(!isStatic)
        {
            /* For non-static methods we got this pointer. */
            methodLoggerDesc += "Ljava/lang/Object;";
        }

        for(String param :  paramTypes)
            methodLoggerDesc += param;

        /* Return void.*/
        methodLoggerDesc += ")V\n";

        return methodLoggerDesc;
    }

    private int calculateNumberOfRegs(boolean isStatic, String[] paramTypes)
    {
        int numRegs = 0;

        if(!isStatic)
            numRegs++;

        for(String param : paramTypes)
        {
            if(param.equals("D"))
                numRegs += 2;
            else
                numRegs++;
        }

        return numRegs;
    }
}
