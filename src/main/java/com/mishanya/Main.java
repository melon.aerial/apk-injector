package com.mishanya;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;

public class Main
{
    private static String apkFilename = "";
    private static String regexpFilename = "";
    public static void main(String[] args)
    {
//        String keyFilename = "my-release-key.keystore";
//        String aliasName = "alias_name";
//        String storePass = "skate8";


        if(!getAndCheckArgs(args))
            return;

        ExecuteApktool executeApktool = new ExecuteApktool();
        SmaliDirectoryInstrumentator smaliDirectoryInstrumentator = new SmaliDirectoryInstrumentator();
        try
        {
            executeApktool.decodeApkFile(apkFilename);
            SmaliDirectoryInstrumentator.instrumentSmaliFolder("./apkOutDir/smali/");
//            if(!executeApktool.buildApkFileAndCheckExistance(apkFilename))
//            {
//                System.out.println("Error building apk file.");
//                return;
//            }
//            String instumentedFilename = executeApktool.cloneFile(apkFilename);
//            //executeApktool.signFile(instumentedFilename, keyFilename, aliasName, storePass);
//            executeApktool.cleanup();
            System.out.println("Everything ok!!!");
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

//        SmaliInstrumentatorHelper helper = new SmaliInstrumentatorHelper();
//        helper.setLoggerFilePath("com/Logger");
//        SmaliClassInstrumentator classInstrumentator = new SmaliClassInstrumentator(helper, "test.smali", "test.smali2");
//        try {
//            classInstrumentator.createInstrumentedClassFile();
//        }catch (IOException e)
//        {
//            e.printStackTrace();
//        }

    }

    private static boolean getAndCheckArgs(String[] args)
    {
        if(args.length != 1)
        {
            System.out.println("Too much or less arguments.\nUsage: apk-injector <apk-file> <regexp-file>");
            return false;
        }

        apkFilename = args[0];
        //regexpFilename = args[1];


        /* Check files existance.*/
        if(!Files.exists(new File(apkFilename).toPath()))
        {
            System.out.println("Error apk file not exists: " + apkFilename);
            return false;
        }

//        if(!Files.exists(new File(regexpFilename).toPath()))
//        {
//            System.out.println("Error regexp file not exists: " + regexpFilename);
//            return false;
//        }

        return true;
    }
}
