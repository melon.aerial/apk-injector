package com.mishanya;
import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

public class SmaliDirectoryInstrumentator
{
    public static void instrumentSmaliFolder(String smaliFolder) throws IOException
    {
        File smaliFolderFile = new File(smaliFolder);
        ArrayList<File> outFiles = new ArrayList<>();
        ArrayList<String> dirs = new ArrayList<>();
        ArrayList<String> relativeDirs = new ArrayList<>();
        Queue<File> tempFiles = new ArrayDeque<>();
        File[] fileList = smaliFolderFile.listFiles();
        if(fileList == null)
            throw new IOException("Error read list of files of folder " + smaliFolderFile.getAbsolutePath());

        for(File file : fileList)
        {
            if(file.isDirectory() && !file.getName().equals("android"))
                tempFiles.add(file);
        }

        while(!tempFiles.isEmpty())
        {
            File folder = tempFiles.poll();
            outFiles.add(folder);

            /* Check subdirs of that folder.*/
            File[] files = folder.listFiles();
            if(files == null)
                throw new IOException("Error read list of files of folder " + folder.getAbsolutePath());

            for(File file : files)
                if(file.isDirectory())
                    tempFiles.add(file);
        }

        /* Make list with paths to folders and relative paths.*/
        Path smaliPath = Paths.get(smaliFolder);
        for(File file : outFiles)
        {
            dirs.add(file.getAbsolutePath());
            Path filePath = file.toPath();
            relativeDirs.add(smaliPath.relativize(filePath).toString());
        }

        for(int i = 0; i < dirs.size(); i++)
        {
            instrumentAllSmaliFilesInDirectory(dirs.get(i), relativeDirs.get(i));
        }
    }

    public static void instrumentAllSmaliFilesInDirectory(String dir, String relativeSmaliDir) throws IOException
    {
        String[] smaliFiles;
        SmaliDirectoryReader dirReader = new SmaliDirectoryReader(dir);
        try
        {
            dirReader.readAllSmaliFilesInDir();
            smaliFiles = dirReader.getSmaliFiles();
            if(smaliFiles.length > 0)
            {
                String loggerClassname = getLoggerClassname(dir);
                SmaliInstrumentatorHelper helper = new SmaliInstrumentatorHelper();
                helper.setLoggerFilePath(relativeSmaliDir + "/" + loggerClassname);
                SmaliInstrumentator instrumentator = new SmaliInstrumentator(smaliFiles, helper);
                instrumentator.processFiles();
                insertLoggerFile(loggerClassname, dir, relativeSmaliDir);
            }
        }
        catch (IOException e)
        {
            throw new IOException("Error instrumenting files in dir :" + dir, e);
        }
    }

    public static String getLoggerClassname(String dir) throws IOException
    {
        String loggerClassname = "Logger";
        String outFile = dir + "/" + loggerClassname + ".smali";

        /* If file exists try to add some postfix.*/
        if(new File(outFile).exists())
        {
            boolean isPostfixFound = false;
            Random randomGenerator = new Random();
            for(int i = 0; i < 10; i++)
            {
                int randomInt = randomGenerator.nextInt(65535);
                loggerClassname = "Logger" + randomInt;
                outFile = dir + "/" + loggerClassname + ".smali";
                if(!new File(outFile).exists())
                {
                    isPostfixFound = true;
                    break;
                }
            }

            if(!isPostfixFound)
                throw new IOException("Failed to generate name for Logger.smali file to insert it in dir : " + dir);
        }

        return loggerClassname;
    }

    public static void insertLoggerFile(String loggerClassname, String dir, String relativeSmaliDir) throws IOException
    {
        File file = new File(ClassLoader.getSystemResource("Logger.smali").getFile());
        FileInputStream fileStream = new FileInputStream(file);
        BufferedReader reader = new BufferedReader(new InputStreamReader(fileStream));
        String outFile = dir + "/" + loggerClassname + ".smali";

        try(PrintWriter out = new PrintWriter(outFile))
        {
            out.println(".class public L" + relativeSmaliDir + "/" + loggerClassname + ";");
            String line;
            while((line = reader.readLine()) != null)
            {
                out.println(line);
            }
        }
        catch (IOException e)
        {
            throw new IOException("Error creating logger file " + outFile);
        }
    }
}

