package com.mishanya;

import java.io.IOException;
import java.nio.file.Files;
import java.io.File;

public class SmaliInstrumentator
{
    private String[] m_smaliFiles;
    SmaliInstrumentatorHelper m_helper;

    SmaliInstrumentator(String[] smaliFiles, SmaliInstrumentatorHelper helper)
    {
        m_smaliFiles = smaliFiles;
        m_helper = helper;
    }

    public void processFiles() throws IOException
    {
        createInstrumentedFiles();
        deleteOldSmaliFiles();
        copyInstrumentedFiles();
    }

    private void createInstrumentedFiles() throws IOException
    {
        SmaliClassInstrumentator classInstrumentator = new SmaliClassInstrumentator(m_helper, "", "");
        for(String fileName : m_smaliFiles)
        {
            classInstrumentator.setInputSmaliFilename(fileName);
            classInstrumentator.setOutputSmaliFilename(fileName + 2);
            classInstrumentator.createInstrumentedClassFile();
        }
    }

    private void deleteOldSmaliFiles() throws IOException
    {
        for(String fileName : m_smaliFiles)
            Files.deleteIfExists(new File(fileName).toPath());
    }

    private void copyInstrumentedFiles() throws IOException
    {
        for(String fileName : m_smaliFiles)
            Files.move(new File(fileName + 2).toPath(), new File(fileName).toPath());
    }
}