package com.mishanya;

import java.io.*;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SmaliClassInstrumentator
{
    private String m_inputSmaliFilename;
    private String m_outputSmaliFilename;
    private String m_className;
    private String m_firstMethodBodyString;
    private SmaliInstrumentatorHelper m_smaliInstrumentatorHelper;

    SmaliClassInstrumentator(SmaliInstrumentatorHelper smaliInstrumentatorHelper, String inputFilename,
                             String outputFileName)
    {
        m_inputSmaliFilename        = inputFilename;
        m_outputSmaliFilename       = outputFileName;
        m_smaliInstrumentatorHelper = smaliInstrumentatorHelper;
    }

    void setOutputSmaliFilename(String outputSmaliFilename)
    {
        m_outputSmaliFilename = outputSmaliFilename;
    }

    void setInputSmaliFilename(String inputSmaliFilename)
    {
        m_inputSmaliFilename = inputSmaliFilename;
    }

    public void createInstrumentedClassFile() throws IOException
    {
        File outFile = new File(m_outputSmaliFilename);
        if(outFile.exists())
        {
            /* TODO: Delete from here delete and make such kind of stuff before instrumentations, if need.*/
            boolean rez = outFile.delete();
            if(outFile.delete())
            {
                throw new IOException("Error in instrumentation of file " + m_inputSmaliFilename
                        + " : output file already exists, couldn't delete it.");
            }
        }

        FileInputStream fileStream = new FileInputStream(m_inputSmaliFilename);
        BufferedReader reader = new BufferedReader(new InputStreamReader(fileStream));

        try(PrintWriter out = new PrintWriter(outFile))
        {
            m_className = readAndWriteClassName(reader, out);
            String str;
            while((str = reader.readLine()) != null)
            {
                if(str.startsWith(".method"))
                {
                    instrumentMethod(str, reader, out);
                }
                else
                    out.println(str);
            }
        }
        catch(IOException e)
        {
            throw new IOException("Error in instrumentating file " + m_inputSmaliFilename, e);
        }
    }

    private void instrumentMethod(String methodFirstString, BufferedReader reader, PrintWriter out) throws IOException
    {
        String[] paramTypes = parseParamTypes(methodFirstString);
        boolean isStatic = parseIsStaticMethod(methodFirstString);
        boolean isNative = parseIsNativeMethod(methodFirstString);
        boolean isAbstract = parseIsAbstractMethod(methodFirstString);
        String methodName = parseMethodName(methodFirstString);

        /* Print first string of method.*/
        out.println(methodFirstString);

        /* Special case for native and abstract methods.*/
        if(methodName.equals("<init>") || methodName.equals("<clinit>") || isNative || isAbstract)
        {
            m_firstMethodBodyString = "";
            readMethodUntilTheEnd(m_firstMethodBodyString, methodName, reader, out);
            return;
        }

        ArrayList<String> paramNamesList = new ArrayList<>();
        ArrayList<Integer> paramRegistersList = new ArrayList<>();
        parseParamRegisterAndOtherStuff(reader, out, methodName, paramRegistersList, paramNamesList);
        if(paramNamesList.size() != paramRegistersList.size())
            throw new IOException("Error in logic saving info about parameters in file " + m_inputSmaliFilename
                    + " in method " + methodName + " .");

        String[] paramNames;
        int[] paramRegisters;

        if(paramNamesList.size() == paramTypes.length)
        {
            /* Make arrays from lists.*/
            paramNames = paramNamesList.toArray(new String[paramNamesList.size()]);
            paramRegisters = new int[paramRegistersList.size()];
            for(int i = 0; i < paramRegisters.length; i++)
            {
                paramRegisters[i] = paramRegistersList.get(i);
            }
        }
        else
        {
            /* Make default arrays for registers and its names from types array.*/
            paramNames = makeParamNamesFromTypes(isStatic, paramTypes);
            paramRegisters = makeParamRegistersFromTypes(isStatic, paramTypes);
        }
        /* TODO: Add variant when some debug information exists, but not all.*/

        /* Check that's everything ok.*/
        if((paramRegisters.length != paramNames.length) || (paramRegisters.length != paramTypes.length))
            throw new IOException("paramRegisters size not equal paramTypes size in method " + methodName);

        insertInstrumentationString(out, methodName, isStatic, paramRegisters, paramTypes);
        readMethodUntilTheEnd(m_firstMethodBodyString, methodName, reader, out);
        insertInstrumentationMethod(out, methodName, isStatic, paramNames, paramRegisters, paramTypes);
    }


    private String[] makeParamNamesFromTypes(boolean isStatic, String[] paramTypes) throws IOException
    {
        String[] ret = new String[paramTypes.length];
        int regNum = 1;
        if(isStatic)
            regNum = 0;

        for(int i = 0; i < paramTypes.length; i++)
        {
            ret[i] = "p" + regNum;
            regNum += getSizeOfTypeInRegs(paramTypes[i]);
        }

        return ret;
    }

    private int[] makeParamRegistersFromTypes(boolean isStatic, String[] paramTypes) throws IOException
    {
        int[] ret = new int[paramTypes.length];
        int regNum = 1;
        if(isStatic)
            regNum = 0;

        for(int i = 0; i < paramTypes.length; i++)
        {
            ret[i] = regNum;
            regNum += getSizeOfTypeInRegs(paramTypes[i]);
        }

        return ret;
    }

    private int getSizeOfTypeInRegs(String type) throws IOException
    {
        char firstLetter = type.charAt(0);
        switch (firstLetter)
        {
            case 'D':
            case 'J':
                return 2;
            case 'I':
            case 'F':
            case 'Z':
            case 'B':
            case 'S':
            case 'C':
            case 'L':
            case '[':
                return 1;
            default:
                throw new IOException("Couldn't define size of primitive type " + type);
        }
    }

    private void insertInstrumentationMethod(PrintWriter out, String methodName, boolean isStatic,
                                             String[] paramNames, int[] paramRegisters,
                                             String[] paramTypes) throws IOException
    {
        try
        {
            String loggerMethod = m_smaliInstrumentatorHelper.createMethodLogger(isStatic, m_className, methodName,
                    paramNames, paramRegisters, paramTypes);
            out.println("");
            out.println("#instumentation method for logging the call values.");
            out.println(loggerMethod);
            out.println("");
        }
        catch (Exception e)
        {
            throw new IOException(e);
        }
    }

    private void insertInstrumentationString(PrintWriter out, String methodName, boolean isStatic,
                                             int[] paramRegisters, String[] paramTypes)
    {
        String invokeStr = m_smaliInstrumentatorHelper.createInvokeOfMethodLogger(isStatic, m_className, methodName,
                paramRegisters, paramTypes);
        out.println(invokeStr);
    }

    private void readMethodUntilTheEnd(String firstMethodBodyString, String methodName, BufferedReader reader,
                                       PrintWriter out) throws IOException
    {
        out.println(firstMethodBodyString);

        /* Check that it's not empty method.*/
        if(firstMethodBodyString.trim().startsWith(".end method"))
            return;

        String nextStr;
        boolean isEndFind = false;
        while((nextStr = reader.readLine()) != null)
        {
            if(nextStr.trim().startsWith(".end method"))
            {
                isEndFind = true;
                out.println(nextStr);
                break;
            }
            out.println(nextStr);
        }

        if(!isEndFind)
            throw new IOException("End of method not found in file " + m_inputSmaliFilename + " for method "
                    + methodName + " .");
    }

    private void parseParamRegisterAndOtherStuff(BufferedReader reader, PrintWriter out, String methodName,
                                                 ArrayList<Integer> paramRegistersList,
                                                 ArrayList<String> paramNamesList) throws IOException
    {
        String nextStr;
        m_firstMethodBodyString = null;
        while((nextStr = reader.readLine()) != null)
        {
            String trimStr = nextStr.trim();

            /* Skip empty lines.*/
            if(trimStr.isEmpty())
            {
                out.println(nextStr);
                continue;
            }

            if(trimStr.charAt(0) != '.' || trimStr.contains(".end method"))
            {
                /* This is first not param string.*/
                m_firstMethodBodyString = nextStr;
                break;
            }

            if(trimStr.startsWith(".param"))
            {
                Integer paramNum = getParamNumberFromStr(trimStr);
                String paramName = getParamNameFromStr(trimStr, paramNum);
                paramRegistersList.add(paramNum);
                paramNamesList.add(paramName);
            }

            if(trimStr.startsWith(".annotation"))
                readAnnotationUntilEnd(methodName, nextStr, reader, out);
            else
                out.println(nextStr);
        }

        if(m_firstMethodBodyString == null)
            throw new IOException("Error in parsing file " + m_inputSmaliFilename +
                    " in finding first string for method " + methodName + " .");
    }

    private void readAnnotationUntilEnd(String methodName, String firstAnnotationStr, BufferedReader reader,
                                        PrintWriter out) throws IOException
    {
        out.println(firstAnnotationStr);

        String nextStr;
        boolean isEndOfAnnotationFound = false;
        while((nextStr = reader.readLine()) != null)
        {
            out.println(nextStr);

            String trimStr = nextStr.trim();
            if(trimStr.startsWith(".end annotation"))
            {
                isEndOfAnnotationFound = true;
                break;
            }
        }

        if(!isEndOfAnnotationFound)
            throw new IOException("Error in fidning end of annotation in file " + m_inputSmaliFilename +
                    " in finding first string for method " + methodName + " .");
    }

    String getParamNameFromStr(String trimStr, Integer paramNum) throws IOException
    {
        Pattern pattern = Pattern.compile(",\\s+\"(\\S+)\"");
        Matcher matcher = pattern.matcher(trimStr);
        if(matcher.find())
        {
            return matcher.toMatchResult().group(1);
        }
        else
        {
            /* If there is no name in param, then make name from register number.*/
            return "p" + paramNum;
        }
    }


    Integer getParamNumberFromStr(String trimStr) throws IOException
    {
        Pattern pattern = Pattern.compile("\\.param\\s+p(\\d+)");
        Matcher matcher = pattern.matcher(trimStr);
        if(!matcher.find())
            throw new IOException("Error parse parameter number from string : " + trimStr);

        String group = matcher.toMatchResult().group(1);
        Integer ret = Integer.valueOf(group);

        /* Double check.*/
        if (matcher.find())
            throw new IOException("Find more than expected parameter number in string : " + trimStr);

        return ret;
    }

    private String parseMethodName(String methodFirstString) throws IOException
    {
        Pattern pattern = Pattern.compile("(\\S+)\\(");
        Matcher matcher = pattern.matcher(methodFirstString);

        if (!matcher.find())
            throw new IOException("Error parse method name from string : " + methodFirstString);

        String group = matcher.toMatchResult().group(1);

        /* Double check.*/
        if (matcher.find())
            throw new IOException("Find more than expected metho names in string : " + methodFirstString);

        return group;
    }

    private String[] parseParamTypes(String methodFirstString) throws IOException
    {
        ArrayList<String> ret = new ArrayList<>();

        String typeStr = getTypeStringFromMethodString(methodFirstString);

        /* Check no parameters case.*/
        if(typeStr == null)
            return new String[0];

        makeParametersListFromStr(ret, typeStr);

        return ret.toArray(new String[ret.size()]);
    }

    private void makeParametersListFromStr(ArrayList<String> ret, String typeStr) throws IOException
    {
        int index = 0;
        String typePrefix = "";
        while(index < typeStr.length())
        {
            char typeFirstLetter = typeStr.charAt(index);
            String type = "";
            switch(typeFirstLetter)
            {
                case 'Z':
                case 'B':
                case 'J':
                case 'C':
                case 'D':
                case 'F':
                case 'I':
                case 'S':
                    type = "" + typeFirstLetter;
                    break;
                case '[':
                    typePrefix += "[";
                    index++;
                    break;
                case 'L':
                    type = readObjectTypeFromString(typeStr, index);
                    break;
                default:
                    throw new IOException("");
            }

            /* Some type parameter found.*/
            if(type.length() != 0)
            {
                /* Add type to result list.*/
                String wholeType = typePrefix + type;
                ret.add(wholeType);

                /* Some cycle logic.*/
                typePrefix = "";
                index += type.length();
            }
        }
    }

    private String readObjectTypeFromString(String typeStr, int index) throws IOException
    {
        String ret = "";
        for(int i = index; i < typeStr.length(); i++)
        {
            if(typeStr.charAt(i) == ';')
            {
                ret += ';';
                break;
            }

            ret += typeStr.charAt(i);
        }

        if(ret.charAt(ret.length() - 1) != ';')
            throw new IOException("Error get object type at index : " + index + " from string: " + typeStr);

        return ret;
    }

    private String getTypeStringFromMethodString(String methodFirstString) throws IOException
    {
        int firstIndex = methodFirstString.indexOf('(');
        int lastIndex = methodFirstString.indexOf(')');

        if((firstIndex != -1) && (lastIndex != -1) && (lastIndex > firstIndex))
        {
            /* Check that not empty paramList.*/
            if(firstIndex + 1 == lastIndex)
                return null;

            /* Get parameters types string. it is between '(' and ')' characters.*/
            return methodFirstString.substring(firstIndex + 1, lastIndex);
        }
        else
            throw new IOException("Error parse paramType from string : " + methodFirstString);
    }

    private boolean parseIsStaticMethod(String methodFirstString)
    {
        return methodFirstString.contains(" static ");
    }

    private boolean parseIsNativeMethod(String methodFirstString)
    {
        return methodFirstString.contains(" native ");
    }

    private boolean parseIsAbstractMethod(String methodFirstString)
    {
        return methodFirstString.contains(" abstract ");
    }

    private String readAndWriteClassName(BufferedReader in, PrintWriter out) throws IOException
    {
        String className;
        String firstLine = in.readLine();
        if(!firstLine.contains(".class "))
            throw new IOException("Smali file doesn't start with \".class \"");

        int classNameStartIndex = firstLine.indexOf("L");
        if(classNameStartIndex == -1)
            throw new IOException("Couldn't find start of class name(\"L\" character)");

        className = firstLine.substring(classNameStartIndex);
        if(!className.endsWith(";"))
            throw new IOException("Couldn't find end of class name(\";\" character)");

        out.println(firstLine);
        return className;
    }
}